//
//  Progresshub.swift
//  ReviewApp
//
//  Created by APPLE  on 07/01/20.
//  Copyright © 2020 APPLE . All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import MBProgressHUD

extension UIViewController {

    func showHUD(progressLabel:String){
        DispatchQueue.main.async{
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.label.text = progressLabel
            progressHUD.hide(animated: true, afterDelay: 60)
        }
    }

    func dismissHUD(isAnimated:Bool) {
       
        DispatchQueue.main.async{
            MBProgressHUD.hide(for: self.view, animated: isAnimated)
        }
    }
}
//class progresshub  {
//class func svprogressHudShow(title:String,view:UIViewController) -> Void
//{
//    SVProgressHUD.show(withStatus: title);
//    SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
//    SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
//    view.view.isUserInteractionEnabled = false;
//}
//
//
//class func svprogressHudDismiss(view:UIViewController) -> Void {
//    DispatchQueue.main.async() {
//        //SVProgressHUD.dismiss()
//        SVProgressHUD.dismiss(withDelay: 25)
//        view.view.isUserInteractionEnabled = true
//    }
//
//}
//}
extension ViewController {
     func svprogressHudShow(title:String,view:UIViewController) -> Void
    {
        SVProgressHUD.show(withStatus: title);
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        view.view.isUserInteractionEnabled = false;
    }


     func svprogressHudDismiss(view:UIViewController) -> Void {
        DispatchQueue.main.async() {
            //SVProgressHUD.dismiss()
            SVProgressHUD.dismiss(withDelay: 25)
            view.view.isUserInteractionEnabled = true
        }
    }
   
}
