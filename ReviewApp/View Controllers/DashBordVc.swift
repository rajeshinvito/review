//
//  DashBordVc.swift
//  ReviewApp
//
//  Created by APPLE  on 03/01/20.
//  Copyright © 2020 APPLE . All rights reserved.
//

import UIKit
import MessageUI

class DashBordVc: UIViewController,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate,MFMessageComposeViewControllerDelegate  {
    
    @IBOutlet weak var Nametfref: UITextField!
    @IBOutlet weak var Phonetfref: UITextField!
    @IBOutlet weak var Emailtfref: UITextField!
    @IBOutlet weak var Locationtfref: UITextField!
    @IBOutlet weak var MailBodytfref: TextField!
    @IBOutlet weak var Facebooklinktfref: TextField!
    @IBOutlet weak var Googlelinktfref: TextField!
    @IBOutlet weak var Googllinkcheckimageref: UIImageView!
    @IBOutlet weak var fblinkcheckimageref: UIImageView!
    
    var sendingfacebooklink = String()
    var sendinggooglelink = String()
    var fbboolvalue = false
    var googleboolvalue = false
    var userdatadict = UserDefaults.standard.dictionary(forKey: "Dict")
    var useremail =  String()
    var UserNumber = String()
    var userfirstname = String()
    var Userlastname = String()
    var userBussiness = String()
    var userPassword = String()
    var userLocation = String()
    var userfacebooklink = String()
    var userGooglelink = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
              self.useremail = userdatadict?["Email"] as? String ?? ""
              self.UserNumber = userdatadict?["phone"] as? String ?? ""
              self.userfirstname = userdatadict?["first"] as? String ?? ""
              self.Userlastname = userdatadict?["last"] as? String ?? ""
              self.userBussiness = userdatadict?["Bussiness"] as? String ?? ""
              self.userPassword = userdatadict?["Password"] as? String ?? ""
              self.userLocation = userdatadict?["Location"] as? String ?? ""
              self.userfacebooklink = userdatadict?["FaceBooklink"] as? String ?? ""
              self.userGooglelink = userdatadict?["Googlelink"] as? String ?? ""
        // Do any additional setup after loading the view.
        
        self.MailBodytfref.text = userBussiness
        self.Locationtfref.text = userLocation
        self.Facebooklinktfref.text = userfacebooklink
        self.Googlelinktfref.text = userGooglelink
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func fblinkcheckbtnref(_ sender: Any) {
         print("adhfv")
        if fbboolvalue == false {
            fblinkcheckimageref.image = #imageLiteral(resourceName: "check")
            self.sendingfacebooklink = self.userfacebooklink
            fbboolvalue = true
        }else {
            fblinkcheckimageref.image = #imageLiteral(resourceName: "uncheck")
            self.sendingfacebooklink = ""
            fbboolvalue = false
        }
    }
    
    @IBAction func Googlelinkcheckbtnref(_ sender: Any) {
         print("adhfv")
        if googleboolvalue == false {
             Googllinkcheckimageref.image = #imageLiteral(resourceName: "check")
            self.sendinggooglelink = self.userGooglelink
            googleboolvalue = true
        }else {
            Googllinkcheckimageref.image = #imageLiteral(resourceName: "uncheck")
            self.sendinggooglelink = ""
            googleboolvalue = false
        }
    }
    
    @IBAction func Settingsbtnref(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "EditInfoVC") as! EditInfoVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Submitbtnref(_ sender: Any) {
        print("adhfv")
        
        if googleboolvalue == true || fbboolvalue == true {
            
        if isValidEmail(Emailtfref.text ?? "") == true {
            self.showHUD(progressLabel: "Loading...")
//            if MFMailComposeViewController.canSendMail() {
//                let message:String  = self.useremail
//                let composePicker = MFMailComposeViewController()
//                composePicker.mailComposeDelegate = self
//                composePicker.delegate = self
//                composePicker.setToRecipients([Emailtfref.text ?? ""])
//                composePicker.setSubject(self.userBussiness)
//                composePicker.setMessageBody(message, isHTML: false)
//                self.present(composePicker, animated: true, completion: nil)
//            }
//            else {
//                self .showerrorMessage()
//            }
        self.senddata()
        }else {
            self.dismissHUD(isAnimated: true)
            print("iudgfqiourhqw;o")
            showAlert(Message: "Invalid Data Enter")
        }
        }else {
            showAlert(Message: "please select any link which you want share")
        }
    }
    
    
    
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    
    
    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith didFinishWithResult:MFMailComposeResult, error:Error?) {
        
        switch didFinishWithResult {
        case .cancelled:
            print("Mail cancelled")
            
            break
        case .saved:
            print("Mail saved")
            
            break
        case .sent:
            print("Mail saved")
            
            break
        case .failed:
            print("Mail saved")
            break
        }
        
       // self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true) {
            self.displayMessageInterface()
        }
        
    }
    
    
    
    func showerrorMessage() {
        
        let alertMessage = UIAlertController(title: "Review App", message: "Data Sent Successfully!", preferredStyle: UIAlertController.Style.alert)
        
        let action = UIAlertAction(title:"Okay", style: UIAlertAction.Style.default, handler: nil)
        
        alertMessage.addAction(action)
        
        self.present(alertMessage, animated: true, completion: nil)
        
    }
    
    
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismissHUD(isAnimated: true)
        self.dismiss(animated: true) {
            self.dismissHUD(isAnimated: true)
            self.Nametfref.text = ""
            self.Phonetfref.text = ""
            self.Emailtfref.text = ""
            self.Googllinkcheckimageref.image = #imageLiteral(resourceName: "uncheck")
            self.fblinkcheckimageref.image = #imageLiteral(resourceName: "uncheck")
            self.showerrorMessage()
        }
        
       }
    
    
    func displayMessageInterface() {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = [Phonetfref.text ?? ""]
        composeVC.body = self.userBussiness  + "," + self.sendinggooglelink + "," +  sendingfacebooklink
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }

    
    func senddata() {

             let smtpSession = MCOSMTPSession()
               smtpSession.hostname =  "smtp.gmail.com"
               smtpSession.username = "rajeshinvito@gmail.com"
               smtpSession.password = "ckoqkhffleymtyja"
               smtpSession.connectionType = MCOConnectionType.TLS
               smtpSession.authType = MCOAuthType.saslPlain
               smtpSession.port = 465
             smtpSession.connectionLogger = {(connectionID, type, data) in
                 if data != nil {
                     if let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                         NSLog("Connectionlogger: \(string)")
                     }
                 }
             }
           let builder = MCOMessageBuilder()
           builder.header.to = [MCOAddress(displayName: "Admin", mailbox: Emailtfref.text ?? "") as Any]
             builder.header.from = MCOAddress(displayName: "Admin", mailbox: "rajeshinvito@gmail.com")
            // if(txtSubject.text == "Other"){
               builder.header.subject = self.userBussiness  + self.sendinggooglelink + sendingfacebooklink
           //  }else {
             //  builder.header.subject = txtSubject.text!
           //  }
             builder.htmlBody = self.userBussiness  + "," + self.sendinggooglelink + "," +  sendingfacebooklink + "<br/><br/><br/><br/><br/><br/>"
               // "hi ji" + "<br/><br/><br/><br/><br/><br/>"
             
             let rfc822Data = builder.data()
             let sendOperation = smtpSession.sendOperation(with: rfc822Data!)
             sendOperation?.start { (error) -> Void in
                 if (error != nil) {
                    self.showHUD(progressLabel: "Loading...")
                    self.displayMessageInterface()
                    NSLog("Error sending email: \(error!)")
                 } else {
                    self.showHUD(progressLabel: "Loading...")
                    self.displayMessageInterface()
                 }
             }
         }
      func showAlert(Message : String){
         // create the alert
                let alert = UIAlertController(title: "Review App", message: Message, preferredStyle: UIAlertController.Style.alert)

                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                // show the alert
                self.present(alert, animated: true, completion: nil)
         }
}
