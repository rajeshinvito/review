//
//  RegistrationVC.swift
//  ReviewApp
//
//  Created by APPLE  on 03/01/20.
//  Copyright © 2020 APPLE . All rights reserved.
//

import UIKit
import SVProgressHUD
class RegistrationVC: UIViewController,UITextViewDelegate {
    @IBOutlet weak var FirstNameref: UITextField!
    @IBOutlet weak var LastNameref: UITextField!
    @IBOutlet weak var Emailref: UITextField!
    @IBOutlet weak var Phonenumberref: UITextField!
    @IBOutlet weak var BussinessInforef: UITextView!
    @IBOutlet weak var Passwordtfref: UITextField!
    @IBOutlet weak var Locationtfref: TextField!
    @IBOutlet weak var facebookLinktfref: TextField!
    @IBOutlet weak var googletfref: TextField!
    
    
    
    var Dict = Dictionary<String,Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
   
  
    @IBAction func Submitbtnref(_ sender: Any) {
       self.showHUD(progressLabel: "Loading...")
        
       if validation() == true {
        Dict.updateValue(Emailref.text ?? "", forKey: "Email")
        Dict.updateValue(FirstNameref.text ?? "", forKey: "first")
        Dict.updateValue(LastNameref.text ?? "", forKey: "last")
        Dict.updateValue(Phonenumberref.text ?? "", forKey: "phone")
        Dict.updateValue(BussinessInforef.text ?? "", forKey: "Bussiness")
        Dict.updateValue(Passwordtfref.text ?? "", forKey: "Password")
        Dict.updateValue(Locationtfref.text ?? "", forKey: "Location")
        Dict.updateValue(facebookLinktfref.text ?? "", forKey: "FaceBooklink")
        Dict.updateValue(googletfref.text ?? "", forKey: "Googlelink")
        UserDefaults.standard.set(Dict, forKey: "Dict")
        self.dismissHUD(isAnimated: true)
        moveToNext()
       }else{
        self.dismissHUD(isAnimated: true)
        showAlert(Message : "Something went wrong")
        }
        
    }
    
    
    @IBAction func BackBtnref(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func moveToNext(){
        let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func validation()-> Bool{
        var value = false
        if FirstNameref.text?.isEmpty == true {
            showAlert(Message : "Enter First name")
        }else if LastNameref.text?.isEmpty == true {
            showAlert(Message : "Enter last name")
        }else if Emailref.text?.isEmpty == true {
           showAlert(Message : "Enter Valid Email")
        }else if Phonenumberref.text?.isEmpty == true {
             showAlert(Message : "Enter valid Phone Number")
        }else if BussinessInforef.text?.isEmpty == true {
             showAlert(Message : "Enter your bussiness discription")
        }else if Passwordtfref.text?.count ?? 0 < 8 {
            showAlert(Message : "Enter min 8 didgit password")
        }else if Passwordtfref.text?.isEmpty == true {
             showAlert(Message : "Enter min 8 didgit password")
        }else if Locationtfref.text?.isEmpty == true {
            showAlert(Message : "Enter location")
        }
      //  else if isValidEmail(Emailref.text ?? "") == true{
      //      showAlert(Message : "Enter Valid Email")
        
     //   }
        else if facebookLinktfref.text?.isEmpty == true {
            showAlert(Message : "Enter FaceBook link")
        
        }else if googletfref.text?.isEmpty == true {
            showAlert(Message : "Enter Google link")
        }
        else {
            value = true
        }
        
        return value
    }
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    func showAlert(Message : String){
           let alert = UIAlertController(title: "Review App", message: Message, preferredStyle: UIAlertController.Style.alert)
           alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
           self.present(alert, animated: true, completion: nil)
    }
}
