//
//  EditInfoVC.swift
//  ReviewApp
//
//  Created by APPLE  on 03/01/20.
//  Copyright © 2020 APPLE . All rights reserved.
//

import UIKit

class EditInfoVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var FirstNameref: TextField!
    @IBOutlet weak var LastNameref: TextField!
    @IBOutlet weak var Emailref: TextField!
    @IBOutlet weak var Phonenumberref: TextField!
    @IBOutlet weak var BussinessInforef: UITextView!
    @IBOutlet weak var Locationtfref: TextField!
    @IBOutlet weak var facebookLinktfref: TextField!
    @IBOutlet weak var googletfref: TextField!
    
    var userdatadict = UserDefaults.standard.dictionary(forKey: "Dict")
    var useremail =  String()
    var UserNumber = String()
    var userfirstname = String()
    var Userlastname = String()
    var userBussiness = String()
    var userPassword = String()
    var userLocation = String()
    var userfacebooklink = String()
    var userGooglelink = String()
    var Dict = Dictionary<String,Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showHUD(progressLabel: "Loading...")
        self.navigationController?.navigationBar.isHidden = false
        self.useremail = userdatadict?["Email"] as? String ?? ""
        self.UserNumber = userdatadict?["phone"] as? String ?? ""
        self.userfirstname = userdatadict?["first"] as? String ?? ""
        self.Userlastname = userdatadict?["last"] as? String ?? ""
        self.userBussiness = userdatadict?["Bussiness"] as? String ?? ""
        self.userPassword = userdatadict?["Password"] as? String ?? ""
        self.userLocation = userdatadict?["Location"] as? String ?? ""
        self.userfacebooklink = userdatadict?["FaceBooklink"] as? String ?? ""
        self.userGooglelink = userdatadict?["Googlelink"] as? String ?? ""
        
        self.Emailref.text =  useremail
        self.LastNameref.text  = Userlastname
        self.FirstNameref.text  = userfirstname
        self.Phonenumberref.text  = UserNumber
        self.BussinessInforef.text  = userBussiness
        self.Locationtfref.text = userLocation
        self.facebookLinktfref.text = userfacebooklink
        self.googletfref.text = userGooglelink
        self.dismissHUD(isAnimated: true)
    }
    
    @IBAction func Editbtnref(_ sender: Any) {
        self.showHUD(progressLabel: "Loading...")
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "Dict")
        defaults.synchronize()
        
        if validation() == true {
            self.useremail = Emailref.text ?? ""
            self.userfirstname = FirstNameref.text ?? ""
            self.Userlastname = LastNameref.text ?? ""
            self.UserNumber = Phonenumberref.text ?? ""
            self.userBussiness = BussinessInforef.text ?? ""
            self.userLocation = Locationtfref.text ?? ""
            self.userfacebooklink = facebookLinktfref.text ?? ""
            self.userGooglelink = googletfref.text ?? ""
            
            
            Dict.updateValue(self.useremail, forKey: "Email")
            Dict.updateValue(self.userfirstname, forKey: "first")
            Dict.updateValue(self.Userlastname, forKey: "last")
            Dict.updateValue(self.UserNumber, forKey: "phone")
            Dict.updateValue(self.userBussiness, forKey: "Bussiness")
            Dict.updateValue(self.userPassword, forKey: "Password")
            Dict.updateValue(self.userLocation, forKey: "Location")
            Dict.updateValue(self.userBussiness, forKey: "Bussiness")
            Dict.updateValue(self.userfacebooklink, forKey: "FaceBooklink")
            Dict.updateValue(self.userGooglelink, forKey: "Googlelink")
            UserDefaults.standard.set(Dict, forKey: "Dict")
            self.dismissHUD(isAnimated: true)
            moveToNext()
        }else {
            self.dismissHUD(isAnimated: true)
            showAlert(Message : "Something went wrong")
        }
    }
    
    
    @IBAction func Logoutbtnref(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "Loginstatus")
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "Dict")
        defaults.synchronize()
        moveToHome()
    }
    
    func moveToNext(){
        let vc = self.storyboard?.instantiateViewController(identifier: "DashBordVc") as! DashBordVc
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func moveToHome(){
        let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func validation()-> Bool{
        var value = false
        if FirstNameref.text?.isEmpty == true {
            showAlert(Message : "Enter First name")
        }else if LastNameref.text?.isEmpty == true {
            showAlert(Message : "Enter last name")
        }else if Emailref.text?.isEmpty == true {
            showAlert(Message : "Enter Valid Email")
        }else if Phonenumberref.text?.isEmpty == true {
            showAlert(Message : "Enter valid Phone Number")
        }else if BussinessInforef.text?.isEmpty == true {
            showAlert(Message : "Enter your bussiness discription")
        }else if Locationtfref.text?.isEmpty == true {
            showAlert(Message : "Enter location")
        }
        else if facebookLinktfref.text?.isEmpty == true {
            showAlert(Message : "Enter FaceBook link")
            
        }else if googletfref.text?.isEmpty == true {
            showAlert(Message : "Enter Google link")
        }
        else {
            value = true
        }
        
        return value
    }
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func showAlert(Message : String){
        // create the alert
        let alert = UIAlertController(title: "Review App", message: Message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}
